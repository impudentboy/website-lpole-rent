// ----------------------------------------
// APP IMPORTS 🔒
// ----------------------------------------

// React
import React from 'react';
import ReactDOM from 'react-dom';


// Library's


// Components
import AppPage from 'components/pages/app';



// ----------------------------------------
// APP RENDERS 🔒
// ----------------------------------------

ReactDOM.render(
    <AppPage/>,
    document.getElementById('app')
);