// ----------------------------------------
// PAGE APP  🔒
// ----------------------------------------


//** IMPORTS **//

// React
import React from 'react';
import PropTypes from 'prop-types';

// Lybrary's
import classNames from 'classnames';

// Styles
import './block.scss';



//** COMPONENT **//

// App Component
class Block extends React.Component {
    constructor(props){
        super(props);

        this.getComponentClassNames = this.getComponentClassNames.bind(this);
    }
    getComponentClassNames(){
        // get classnames and type
        const { className, background } = this.props;

        return { 
            component: classNames(
                className ? className : '',
                'b-block',
                background ? `b-block--${background}` : ''
            )
        } 
    }
    render(){
        // get classes
        const classes = this.getComponentClassNames();
        // get props
        const { children } = this.props;

        return(
            <div className={ classes.component }>
               { children }
            </div>
        );
    }
}

// App PropTypes
Block.propTypes = {
    className: PropTypes.string,
    background: PropTypes.string
}



//** EXPORTS **//

export default Block;