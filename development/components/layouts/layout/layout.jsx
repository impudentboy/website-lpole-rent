// ----------------------------------------
// PAGE APP  🔒
// ----------------------------------------


//** IMPORTS **//

// React
import React from 'react';
import PropTypes from 'prop-types';

// Lybrary's
import classNames from 'classnames';

// Styles
import './layout.scss';



//** COMPONENT **//

// App Component
class Layout extends React.Component {
    constructor(props){
        super(props);

        this.getComponentClassNames = this.getComponentClassNames.bind(this);
    }
    getComponentClassNames(){
        // get classnames and type
        const { className, type } = this.props;

        return { 
            component: classNames(
                className ? className : '',
                'l-layout',
                type ? `l-layout--${type}` : ''
            )
        } 
    }
    render(){
        // get classes
        const classes = this.getComponentClassNames();
        // get props
        const { children } = this.props;

        return(
            <div className={ classes.component }>
               { children }
            </div>
        );
    }
}

// App PropTypes
Layout.propTypes = {
    className: PropTypes.string,
    type: PropTypes.string.isRequired
}

// Layout Default Props
Layout.defaultProps = {
    type: 'app'
}



//** EXPORTS **//

export default Layout;