// ----------------------------------------
// PAGE APP  🔒
// ----------------------------------------


//** IMPORTS **//

// React
import React from 'react';
import PropTypes from 'prop-types';

// Lybrary's
import classNames from 'classnames';

// Styles
import './wrapper.scss';



//** COMPONENT **//

// App Component
class Wrapper extends React.Component {
    constructor(props){
        super(props);

        this.getComponentClassNames = this.getComponentClassNames.bind(this);
    }
    getComponentClassNames(){
        // get classnames and type
        const { className } = this.props;

        return { 
            component: classNames(
                className ? className : '',
                'b-wrapper'
            )
        } 
    }
    render(){
        // get classes
        const classes = this.getComponentClassNames();
        // get props
        const { children } = this.props;

        return(
            <div className={ classes.component }>
               { children }
            </div>
        );
    }
}

// App PropTypes
Wrapper.propTypes = {
    className: PropTypes.string
}



//** EXPORTS **//

export default Wrapper;