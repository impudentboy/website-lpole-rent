// ----------------------------------------
// PAGE APP  🔒
// ----------------------------------------


//** IMPORTS **//

// React
import React from 'react';
import PropTypes from 'prop-types';

// Components
import Layout   from 'components/layouts/layout';
import Block    from 'components/layouts/block';
import Wrapper  from 'components/layouts/wrapper';



//** COMPONENT **//

// App Component
class AppPage extends React.Component {
    constructor(props){
        super(props);
    }
    render(){

        return(
            <Layout>
                <Block background="gray">
                    <Wrapper>
                        Initial
                    </Wrapper>
                </Block>
            </Layout>
        );
    }
}

// App PropTypes
AppPage.propTypes = {
    className: PropTypes.string
}



//** EXPORTS **//

export default AppPage;