//** IMPORTS **//

var webpack = require('webpack');
var path = require('path');
var config = require('./webpack.common.js');

//** CONFIG **//


// output
config.output.publicPath = 'http://127.0.0.1:8090/build/';
config.output.path = path.resolve('./devserver/build/');
// // dev tools
// config.devtool = 'eval-source-map';

// dev server
config.devServer = {
    hot: true
}

config.plugins = config.plugins.concat([
    new webpack.NoEmitOnErrorsPlugin(), // don't reload in case of error
])

config.module.rules.push(
    {
        test: /\.js$/,
        loaders: ["react-hot-loader", "babel-loader"],
        exclude: [/node_modules/, /devserver/],
    },
    {
        test: /\.jsx$/,
        loaders: ["react-hot-loader", "babel-loader"],
        exclude: [/node_modules/, /devserver/],
    }
);

config.devServer = {
    historyApiFallback: true,
};

module.exports = config;