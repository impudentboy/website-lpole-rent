var webpack = require('webpack');
var path = require('path');

module.exports = {
    entry: [
        './development/main.js'
        // the entry point of our app
    ],
    output: {
        filename: 'bundle.js',
        // the output bundle
        
    },
    plugins: [
    ],
    module: {
        rules: [ // all common loaders go here
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            },
            {
		        test: /\.scss$/,
		        loader: "style-loader!css-loader!sass-loader"

		    },
		    {
                test: /\.png$/,
                loader: "url-loader?limit=99999&mimetype=image/png"
            },
            {
                test: /\.jpg$/,
                loader: "url-loader?limit=99999&mimetype=image/jpg"
            },
            {
                test: /\.svg/,
                loader: "url-loader?limit=26000&mimetype=image/svg+xml"
            },
            {
                test: /\.ttf$/,
                loader: "url?limit=65000&mimetype=application/octet-stream&name=public/fonts/[name].[ext]"
            },
            {
                test: /\.eot$/,
                loader: "url?limit=65000&mimetype=application/vnd.ms-fontobject&name=public/fonts/[name].[ext]"
            },
            {
                test: /\.woff$/,
                loader: "url?limit=65000&mimetype=application/font-woff&name=public/fonts/[name].[ext]"
            },
            {
	            test: /\.woff2$/,
	            loader: "url?limit=65000&mimetype=application/font-woff2&name=public/fonts/[name].[ext]"
	        }
        ]
    },
    resolve: {
        alias: {
            //** ALIASES FOR BASE STRUCTURE **//
            actions:    path.resolve(__dirname, './development/actions'),
            assets:     path.resolve(__dirname, './development/assets'),
            reducers:   path.resolve(__dirname, './development/reducers'),
            store:      path.resolve(__dirname, './development/store'),
            components: path.resolve(__dirname, './development/components'),
            containers: path.resolve(__dirname, './development/containers'),
        }
    }
}