var webpack = require('webpack');
var path = require('path');
var config = require('./webpack.common.js');


config.output.publicPath = 'production/build/';
config.output.path = path.resolve('production/build/');

config.module.rules.push(
    {
        test: /\.js$/,
        loaders: ["babel-loader"],
        exclude: [/node_modules/, /devserver/],
    },
    {
        test: /\.jsx$/,
        loaders: ["babel-loader"],
        exclude: [/node_modules/, /devserver/],
    }
);

module.exports = config;