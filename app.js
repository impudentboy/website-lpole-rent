var http = require('http');
var express = require('express');
var app = express();

var port = (process.env.PORT || 9080);


//**  SERVER  **//

app.use(express.static('production'));
app.use(express.static(__dirname));

app.listen(port, function () {
  console.log(`Example app listening on port ${port}!`);
});